# Selamat Datang di Repositori Desain

Proyek desian pada repositori ini adalah proyek **hobi** yang saya kerjakan
dengan bantuan alat atau perangkat lunak yang bernama Inkscape maupun GIMP. Kedua
aplikasi tersebut saya jalankan di [Ubuntu MATE](https://ubuntu-mate.org/).

# Lisensi

[![Lisensi Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Ciptaan disebarluaskan di bawah [Lisensi Creative Commons Atribusi-BerbagiSerupa 4.0 Internasional](http://creativecommons.org/licenses/by-sa/4.0/)

## Maksud dari Lisensi; Atribusi-BerbagiSerupa

Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat
ciptaan turunan bahkan untuk kepentingan komersial, selama mereka mencantumkan
kredit kepada Anda dan melisensikan ciptaan turunan di bawah syarat yang serupa.
Lisensi ini seringkali disamakan dengan lisensi "copyleft" pada perangkat lunak
bebas dan terbuka. Seluruh ciptaan turunan dari ciptaan Anda akan memiliki lisensi
yang sama, sehingga setiap ciptaan turunan dapat digunakan untuk kepentingan komersial.

# Petunjuk

Petunjuk penggunaan:

- Saya merekomendasikan penggunaan aplikasi [Inkscape](https://inkscape.org/) untuk keperluan pengeditan (berkas .svg).
- Saya merekomendasikan penggunaan aplikasi [GIMP](https://www.gimp.org/) untuk keperluan pengeditan (berkas .xcf).
- Penggunaan sistem operasi GNU/Linux untuk menjalankan kedua aplikasi tersebut lebih diutamakan.
- Hormati semua ketentuan penggunaan ciptaan sesuai dengan lisensi yang disebutkan di atas.


# Persiapan Pemakaian

Bagian ini menjelaskan persiapan dalam melakukan editing yang mungkin Anda butuhkan.

## Memasang Aplikasi

Pasang Inkscape dan GIMP di sistem operasi GNU/Linux Anda dengan perintah berikut:

```sh
$ sudo apt install inkscape
$ sudo apt install gimp
```
atau
```sh
$ sudo apt install inkscape gimp
```

## Memperoleh Fonta

Karya dalam repositori ini mayoritas menggunakan fonta Roboto, Roboto Mono, Fira Sans dan FontAwesome (untuk keperluan ikon).
Fonta tersebut dapat diperoleh secara bebas di situs [Google Fonts](https://fonts.google.com/), khusus untuk keperluan ikon,
dapat Anda unduh di stus [FontAwesome](https://fontawesome.com/download), unduh FontAwesome untuk desktop.

Anda juga dapat memasang FontAwesome di sistem operasi GNU/Linux turunan Debian dengan mengeksekusi
perintah beriku:

```sh
$ sudo apt install fonts-font-awesome
```

# Akhir Kata

Selamat menikmati ^^